__author__ = 'serjantiquity'
from xmlrpc import client
from config import url


class Login():
    def __init__(self, url=url, db=None, username=None, password=None):
        self.url = url
        self.db = db
        self.username = username
        self.password = password

    def getUserId(self):
        """
        :return: user ID
        """
        common = client.ServerProxy('{}/xmlrpc/2/common'.format(self.url))
        return common.authenticate(self.db, self.username, self.password, {})



if __name__ == '__main__':
    login = Login(url, 'todogle', 'admin', 'masterkey')
    print(login.getUserId())
