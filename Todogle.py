from flask import Flask, url_for, request, render_template, session, redirect, flash
from odoo_connector import connector

app = Flask(__name__)
app.config.from_object("config")


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/logout')
def logout():
    session.pop('logged_in', None)
    flash('You were logged out', "global")
    return redirect(url_for('index'))


@app.route('/login', methods=['GET', 'POST'])
def login():
    error = None
    if request.method == 'POST':
        username = request.form['username'] 
        password = request.form['password']
        db = request.form['Database']
        if username and password and db:
            try:
                connect = connector.Login(username=username, password=password, db=db)
                uuid = connect.getUserId()
                session['logged_in'] = True
                flash(uuid, "global")
            except Exception as e:
                flash(e, "global")
        else:
            flash('incorrect input data', "global")
    return render_template('login.html', error=error)


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=8080)
