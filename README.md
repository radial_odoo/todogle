#Требования:
Необходимо, что бы были установлены модули 
xmlrpc  и flask

#Для запуска:

*1. Скопировать себе код: $ git clone git://Radialadmin@bitbucket.org/radial_odoo/todogle.git*

*2. Перейти в каталог "Todogle"*

*3. Выполнить комманду "python3 Todogle.py"*

*4. В браузере открыть страничку "127.0.0.1:8080"*

*5. Ввести логин (tester), пароль (test2015) и базу данных (Todogle)*